from flask import Blueprint, request, abort
from . import db
from uuid import uuid4
from time import time
from json import dumps

bp = Blueprint("player", __name__, url_prefix="/player")

@bp.route("/", methods=("POST"))
def post_player():
    conn = db.get_conn()
    cursor = conn.cursor()
    id = uuid4()
    name = request.form["name"]
    timestamp = time()
    cursor.execute("INSERT INTO players (uuid, name, created) VALUES (?, ?, ?)", (id, name, timestamp))
    return dumps({"uuid": id, "name": name, "created": timestamp})

@bp.route("/<id>", methods=("GET", "PUT", "DELETE"))
def player(id):
    conn = db.get_conn()
    cursor = conn.cursor()
    if request.method == "PUT":
        pass
    elif request.method == "GET":
        p = cursor.execute("SELECT * FROM players WHERE uuid=?", (id,)).fetchone()
        if p is None:
            abort(404)
        else:
            return p