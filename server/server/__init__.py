from flask import Flask
from markupsafe import escape
import logging, logging.config
import os

logging.config.dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

app = Flask(__name__, instance_relative_config=True)
app.config.from_mapping(SECRET_KEY='dev', DATABASE=os.path.join(app.instance_path, "rougerogue.db"))

from . import player
app.register_blueprint(player.bp)

@app.route("/hello/<name>")
def hello(name):
    return f"Hello, {escape(name)}!"