CREATE TABLE IF NOT EXISTS players (
    uuid text NOT NULL PRIMARY KEY,
    name text NOT NULL,
    created datetime NOT NULL,
    FOREIGN KEY(team) REFERENCES teams(uuid)
);

CREATE TABLE IF NOT EXISTS users (
    uuid text NOT NULL PRIMARY KEY,
    password text NOT NULL,
    admin boolean DEFAULT 0,
    FOREIGN KEY(idol) REFERENCES players(uuid)
);

CREATE TABLE IF NOT EXISTS teams (
    uuid text NOT NULL PRIMARY KEY,
    name text NOT NULL,
    slogan text,
    created datetime NOT NULL,
    FOREIGN KEY(owner) REFERENCES users(uuid)
);