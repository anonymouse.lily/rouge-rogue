import requests, click

@click.group()
def group():
    pass

@group.command()
@click.argument("name")
def hello(name):
    r = requests.get(f"http://127.0.0.1:5000/hello/{name}")
    click.echo(r.content)

@group.group()
def player():
    pass

@player.command("get")
@click.argument("id")
def get_player(id):
    r = requests.get(f"http://127.0.0.1:5000/player")
    click.echo(r.content)

if __name__ == "__main__":
    group()